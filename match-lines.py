import argparse
import re
import string

import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

nltk.download("wordnet")
nltk.download("stopwords")

# Define and parse command-line arguments
parser = argparse.ArgumentParser(
    description="Find closest matching lines between two files."
)
parser.add_argument("file1", type=str, help="Path to the first file.")
parser.add_argument("file2", type=str, help="Path to the second file.")
parser.add_argument(
    "--pattern1",
    type=str,
    default="(.+)",
    help="Regex pattern to extract sub-text to match from file 1. Should contain one group for description extraction.",
)
parser.add_argument(
    "--pattern2",
    type=str,
    default="(.+)",
    help="Regex pattern to extract sub-text to matchfrom file 2. Should contain one group for description extraction.",
)
args = parser.parse_args()


def read_description(line, pattern):
    try:
        return re.search(pattern, line).group(1)
    except Exception:
        raise Exception(f"{line} does not match {pattern}")


def remove_punctuation(text):
    return text.translate(str.maketrans("", "", string.punctuation))


stemmer = PorterStemmer()
stop_words = set(stopwords.words("french"))


def prepare(text):
    text = text.lower()
    text = remove_punctuation(text)
    tokens = text.split()
    stemmed_tokens = [stemmer.stem(token) for token in tokens]
    filtered_tokens = [token for token in stemmed_tokens if token not in stop_words]
    processed_text = " ".join(filtered_tokens)
    return processed_text


# Function to read descriptions from a file using regex pattern
def read_descriptions(filepath, pattern):
    with open(filepath, "r", encoding="utf-8") as file:
        lines = file.readlines()
        descriptions = [prepare(read_description(line, pattern)) for line in lines]
    return lines, descriptions


table_1_lines, table_1_descriptions = read_descriptions(args.file1, args.pattern1)
table_2_lines, table_2_descriptions = read_descriptions(args.file2, args.pattern2)

# Combine all descriptions to compute a TF-IDF representation
all_descriptions = table_1_descriptions + table_2_descriptions
vectorizer = TfidfVectorizer().fit(all_descriptions)
all_descriptions_tf_idf = vectorizer.transform(all_descriptions)

# Split the combined TF-IDF matrix into two for each table
table_1_data = all_descriptions_tf_idf[: len(table_1_descriptions)]
table_2_data = all_descriptions_tf_idf[len(table_1_descriptions) :]

# Compute cosine similarity between tables
similarity_matrix = cosine_similarity(table_1_data, table_2_data)

# Find the closest match from table 2 for each description in table 1
matches = {}
for idx, row in enumerate(similarity_matrix):
    best_match_index = row.argmax()
    matches[table_1_lines[idx].strip()] = table_2_lines[best_match_index].strip()

# Print matches
for line_1, line_2 in matches.items():
    print(f"{line_1} -> {line_2}")
