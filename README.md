# match-lines

Use scikit-learn's cosine similarity computation to match lines between two files.

## Usage

```
% python ./match-lines.py --help
usage: match-lines.py [-h] [--pattern1 PATTERN1] [--pattern2 PATTERN2] file1 file2

Find closest matching lines between two files.

positional arguments:
  file1                Path to the first file.
  file2                Path to the second file.

options:
  -h, --help           show this help message and exit
  --pattern1 PATTERN1  Regex pattern to extract sub-text to match from file 1. Should contain one group for
                       description extraction.
  --pattern2 PATTERN2  Regex pattern to extract sub-text to match from file 2. Should contain one group for
                       description extraction.
```
